import re
import sys

PLAYBOOK_NAME_REGEX = r'^[a-z][a-z0-9\-]+$'

playbook_name = '{{ cookiecutter.playbook_name }}'

if not re.match(PLAYBOOK_NAME_REGEX, playbook_name):
    print('ERROR: "{}" is not a valid playbook name! It should match "^[a-z][a-z0-9\-]+$"'.format(playbook_name))
    sys.exit(1)
